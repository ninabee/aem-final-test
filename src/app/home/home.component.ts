import { Component} from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent{

  username:string="user@aemenersol.com";
  password:string="Test@123";

  constructor( private http:Http, private storage:Storage, private router:Router ) {
    this.storage.get('token').then( async (token)=>{
      if(token==''||token==null){  }
      else{ this.router.navigateByUrl('dashboard'); }
    });  
  }

  async submit(){
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let data = {username:this.username, password:this.password};
    let options = new RequestOptions({ headers: headers });
    await this.http.post("http://test-demo.aem-enersol.com/api/account/login", data, options)
    .map(res => res.json())
    .subscribe( 
        (res)=>{ this.storage.set('token',res).then(()=>{this.router.navigateByUrl('dashboard');});  }, 
        (err)=>{ alert(JSON.stringify(err)); } 
    );
  }  


}
