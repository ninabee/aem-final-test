import { Component, NgZone } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent{

  chartdonut:any=[]; chartbar:any=[]; tableusers:any=[];

  constructor( private http:Http, private storage:Storage, private router:Router,
               private zone: NgZone ) { 
    this.storage.get('token').then( async (token)=>{
      if(token==''||token==null){ this.router.navigateByUrl('home'); }
      else{ 
         var headers = new Headers();
         headers.append("Accept", 'application/json');
         headers.append('Content-Type', 'application/json');
         headers.append('Authorization', "Bearer " + token);
         let options = new RequestOptions({ headers: headers });
         await this.http.get("http://test-demo.aem-enersol.com/api/dashboard", options)
         .map(res => res.json())
         .subscribe( (res)=>{ 
              this.donutcharts(res["chartDonut"]);
              this.xycharts(res["chartBar"]);
              this.tableusers=res["tableUsers"];
         },(err)=>{ alert(JSON.stringify(err)); });
      }
    });
  }

  signout(){ this.storage.clear().then( ()=>{this.router.navigateByUrl('home');} ); }

  ngOnInit() {
  }

  donutcharts(value:any) {
    this.zone.runOutsideAngular(() => {
      var piechart = am4core.create("piechart", am4charts.PieChart);
      piechart.data = value; 
      piechart.innerRadius = am4core.percent(50);
      var pieSeries = piechart.series.push(new am4charts.PieSeries());
      pieSeries.dataFields.value = "name";
      pieSeries.dataFields.category = "value";
      pieSeries.slices.template.stroke = am4core.color("#fff");
    });
  }

  xycharts(value:any) {
    this.zone.runOutsideAngular(() => {
      var xychart = am4core.create("xychart", am4charts.XYChart);
      xychart.data = value;           
      var categoryAxis = xychart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "name";
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.minGridDistance = 30;    
      var valueAxis = xychart.yAxes.push(new am4charts.ValueAxis());
      var series = xychart.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueY = "value";
      series.dataFields.categoryX = "name";
    });
  }
 
}
